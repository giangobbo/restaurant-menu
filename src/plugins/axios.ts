import axios from 'axios';

export default axios.create({
  timeout: 30000,
  // You can add your headers here
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
    'Content-Type': 'application/json',
    Accept: '*/*',
  },
});
