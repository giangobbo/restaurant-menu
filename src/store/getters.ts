import {
  IBasketItem, ICategory, IItem, IState,
} from './types';

export default {
  getBasketItems(state: IState): IBasketItem[] {
    return state.basket.getItems();
  },
  getItems(state: IState): IItem[] {
    return state.items.filter((item) => item.name.toLowerCase().includes(state.searchString.toLowerCase()));
  },
  getCategories(state: IState): ICategory[] {
    return state.categories;
  },
  getBasketTotal(state: IState): number {
    return state.basket.getTotal();
  },
  getError(state: IState): string {
    return state.error;
  },
};
