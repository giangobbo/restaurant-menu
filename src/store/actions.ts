import { ActionContext } from 'vuex';
import axios from 'axios';
import {
  IState, IBasketItem,
} from './types';

export default {

  addItemToBasket(store: ActionContext<IState, any>, item: IBasketItem): void {
    store.commit('addItemToBasket', item);
  },
  fetchMenu(store: ActionContext<IState, any>): void {
    axios.get(`${process.env.VUE_APP_CORS_PROXY}https://chatfood-cdn.s3.eu-central-1.amazonaws.com/fe-code-challenge-1/menu.json`)
      .then((response) => {
        store.commit('saveItems', response.data.items);
        store.commit('saveCategories', response.data.categories);
      })
      .catch(
        (error) => {
          store.commit('settingError', error.message);
        },
      );
  },
  closeNotification(store: ActionContext<IState, any>): void {
    store.commit('settingError', '');
  },
  resetApp(store: ActionContext<IState, any>): void {
    store.commit('resetApp');
    store.dispatch('fetchMenu');
  },
};
