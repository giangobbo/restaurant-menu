import { Basket } from './types';

export default {
  basket: localStorage.getItem('basket') ? new Basket((JSON.parse(localStorage.getItem('basket') || '{}')).items) : new Basket([]),
  items: [],
  categories: [],
  error: '',
  showError: false,
  searchString: '',
};
