import {
  Basket, IState, IItem, ICategory,
} from './types';

export default {
  addItemToBasket(state: IState, payload: string):void {
    const item = state.items.filter((i) => i.id === payload);

    if (state.basket.getCountForItem(payload) < (item[0].stock ? item[0].stock.availability : 0)) {
      state.basket.addItem({
        id: item[0].id,
        name: item[0].name,
        quantity: 1,
      });
      localStorage.setItem('basket', JSON.stringify(state.basket));
    } else {
      state.error = `${item[0].name} is out of stock !`;
      state.showError = true;
    }
  },
  saveItems(state: IState, payload: IItem[]):void {
    state.items = payload;
  },
  saveCategories(state: IState, payload: ICategory[]):void {
    state.categories = payload;
  },
  settingError(state: IState, payload: string):void {
    state.error = payload;
    if (payload === '') {
      state.showError = false;
    } else {
      state.showError = true;
    }
  },
  resetApp(state: IState):void {
    state.basket = new Basket([]);
    localStorage.setItem('basket', JSON.stringify(state.basket));
    state.items = [];
    state.categories = [];
    state.error = '';
  },
};
