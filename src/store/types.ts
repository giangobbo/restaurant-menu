export interface IItem {
  id: string,
  name: string,
  url: string,
  price: number,
  // eslint-disable-next-line camelcase
  discount_rate: number,
  stock: {
    availability: number
  },
  description: string,
  photo: string,
  // eslint-disable-next-line camelcase
  category_id: string
}

export interface ICategory {
  id: string,
  name: string,
  url: string,
}

export interface IMenuItem extends ICategory {
  items: IItem[]
}

export interface IBasketItem {
  id: string,
  name: string,
  quantity: number
}

export class Basket {
  private items: IBasketItem[]

  private total = 0;

  // constructor
  constructor(items: IBasketItem[]) {
    this.items = items;
    this.total = items.map((i) => i.quantity).reduce((a, b) => a + b, 0);
  }

  addItem(item: IBasketItem): void{
    let found = false;
    this.items.forEach((i) => {
      if (i.id === item.id) {
        // eslint-disable-next-line no-param-reassign
        i.quantity += item.quantity;
        this.total += item.quantity;
        found = true;
      }
    });
    if (!found) {
      this.items.push(item);
      this.total += item.quantity;
    }
  }

  getCountForItem(itemId: string): number {
    const itemOnCart = this.items.filter((item) => item.id === itemId);
    if (itemOnCart.length > 0) return itemOnCart[0].quantity;
    return 0;
  }

  getTotal(): number {
    return this.total;
  }

  getItems(): IBasketItem[] {
    return this.items;
  }
}

export interface IState {
  basket: Basket,
  items: IItem[],
  categories: ICategory[]
  error: string
  showError: boolean
  searchString: string
}
