import { shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import { IItem } from '@/store/types';
import Dish from '../../src/components/Dish.vue';

Vue.use(Vuetify);

describe('Dish.vue', () => {
  it('renders', () => {
    const items: any = {
      id: '1',
      name: 'test name',
      url: 'test url',
      price: 3000,
      discountRate: 0.1,
      availability: 10,
      description: 'string',
      photo: '',
      category_id: '1',
    };

    const wrapper = shallowMount(Dish, {
      propsData: items,
    });

    expect(wrapper.find('b').text()).toBe(items.name);
    expect(wrapper.find('strike').text()).toBe(`AED${String.fromCharCode(160)}30.00`);
  });
});
