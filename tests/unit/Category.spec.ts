import { shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import { IItem } from '@/store/types';
import Category from '../../src/components/Category.vue';

Vue.use(Vuetify);

describe('Category.vue', () => {
  it('renders with no items', () => {
    const name = 'Test Category';
    const url = 'Test URL';
    const items: IItem[] = [];

    const wrapper = shallowMount(Category, {
      propsData: { name, url, items },
    });

    expect(wrapper.find('h1').text()).toBe(name);
    expect(wrapper.find('i').text()).toBe('no results');
  });

  it('renders with multiple item', () => {
    const name = 'Test Category';
    const url = 'Test URL';
    const items: IItem[] = [
      {
        id: '1',
        name: 'test name',
        url: 'test url',
        price: 100,
        discount_rate: 0.1,
        stock: {
          availability: 10,
        },
        description: 'string',
        photo: '',
        category_id: '1',
      },
      {
        id: '2',
        name: 'test name 2',
        url: 'test url',
        price: 100,
        discount_rate: 0.1,
        stock: {
          availability: 10,
        },
        description: 'string',
        photo: '',
        category_id: '1',
      },
    ];

    const wrapper = shallowMount(Category, {
      propsData: { name, url, items },
    });

    expect(wrapper.find('h1').text()).toBe(name);
    expect(wrapper.find('dish-stub')).toBeTruthy();
  });
});
