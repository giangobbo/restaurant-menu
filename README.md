# Restaurant Menu

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Lints and fixes files

```
npm run lint
```

## Running with Docker

### Build the Image

```
docker build --pull --rm -f "Dockerfile" -t menu:latest "."

```

### Run on port 8000

```
docker run --rm -it -p 8000:80/tcp menu:latest

```

### CORS

```

If the app is acessed through `localhost`, CORS should not be a problem and you should be able to visualize the menu with no problem.

If you want to acess it through another endpoint, like `http://192.168.15.16:8080/`, you'll probably face CORS blocking the request to fetch the menu json. In that case you'll see a `Network Error` notification on the screen.

To fix that error please add `https://cors-anywhere.herokuapp.com/` to the `VUE_APP_CORS_PROXY` variable on the `.env` file.

```

## Design Decisions

### Framework, Libraries and packages

The application has been developed in Typescript, using the Vue.js JavaScript framework, version 2. In addition, the UI has been developed using components from the Vuetify Library and using Boostrap classes.

Main packages used:

- Vuex, to manage the application state.
- Axios, to make http requests.
- Jest, to create and run tests.

### Code Logic

The application is structured as Vue.js components and the data it uses is stored on the application state manager, Vuex, making it easier to integrate in larger applications.

The application entry point is the `src/components/App.vue` component, this component renders the top toolbar, the cart component, the notifications and main component of the application `src/components/Menu.vue`.

When the Menu component is mounted it triggers the Vuex action that fetches the menu items and categories from the api. That data is then stored on Vuex state to be used throughout the application.

The Menu component has a computed property that combines the categories and items information from the application state into a single object, mapping the items of each category in their respective category object. In addition, that same computed property also filters the items according to the user search.

For each category in the computed property an instance of the `src/components/Category.vue` component is rendered, which will display the category name and then render an instance of the `src/components/Dish.vue` for each item of that category. The Dish component will display the information about that item and when clicked will add one item to the cart, which is stored on the application state.

As items are added to the cart, the cart counter on the toolbar will increase and clicking on it the `src/components/Cart.vue` component will open, it is a modal that displays each item on the cart with its quantity. The cart information is also stored on the application state and its kept on the browser local storage.

### Coding style

The coding style chosen for the development of this application is AirBnB’s

The main reason I chose it over the standard coding style is because it improves the code readability in general, and debuging as consequence. This is specially true for TypeScript code, which is inherit more complex than plain Javascript.

In my opinion, the main rules that contributes for that are:

- Spacing and indentation;
- `const` and `let` over var;
- Enforces the declaration before use concept.
- Enforces simplification of arrow functions.

Another important feature is the automation capability, AirBnB's has most of its standards as eslint rules, so its possible to enforce it on the build process and even configure the IDE to automatically pick up errors up as the developer types and auto-fix them if desired.
